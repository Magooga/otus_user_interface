﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace OTUS_user_interface
{
    public class ImageDownloader
    {
        string remoteUri = "https://effigis.com/wpcontent/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg";

        string fileName = "bigimage.jpg";

        //public Func<string, string> f;
        Task dwnld;

        public bool ChkTask
        {
            get
            {
                return dwnld.IsCompleted;
            }
        }

        public event Action<string> ImageStarted;
        public event Action<string> ImageCopleted;

        //public ImageDownloader()
        //{
        //    ImageStarted += ImgStrt;
        //    ImageCopleted += ImgCmplt;
        //}

        public void Download(string remoteUri)
        {
            var myWebClient = new WebClient();

            if (ImageStarted != null)
                //Console.WriteLine("Качаю \"{0}\" из \"{1}\" .......\n\n", fileName, remoteUri);
                ImageStarted($"Качаю \"{fileName}\" из \"{remoteUri}\" .......\n\n");

            //myWebClient.DownloadFile(remoteUri, fileName);
            dwnld = myWebClient.DownloadFileTaskAsync(remoteUri, fileName);

            if (ImageCopleted != null)
                //Console.WriteLine("Успешно скачал \"{0}\" из \"{1}\"", fileName, remoteUri);
                ImageCopleted($"Успешно скачал \"{fileName}\" из \"{remoteUri}\"");
        }

        public void ImgStrt(string msg)
        {
            Console.WriteLine(msg);
        }

        public void ImgCmplt(string msg)
        {
            Console.WriteLine(msg);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string cmnd;
            bool res;

            ImageDownloader m = new ImageDownloader();

            m.ImageStarted += m.ImgStrt;
            m.ImageCopleted += m.ImgCmplt;

            m.Download("https://mobimg.b-cdn.net/v3/fetch/70/708698fd251a43214d6198c0c6438156.jpeg");

            Console.WriteLine("Press any key...");
            cmnd = Console.ReadLine();

            while (true)
            {
                if (cmnd != "A")
                {
                    res = m.ChkTask;
                    if (res)
                    {
                        Console.WriteLine("Task true - already download");
                        break;
                    }  
                    else
                        Console.WriteLine("Task false - not download");
                }
                else
                    break;
            }


            Console.WriteLine("Press any key...");
            cmnd = Console.ReadLine();
        }
    }
}
