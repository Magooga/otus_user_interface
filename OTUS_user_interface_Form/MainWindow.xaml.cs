﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;


namespace OTUS_user_interface_Form
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public class ImageDownloader
    {
        string remoteUri = "https://effigis.com/wpcontent/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg";

        string fileName = "bigimage.jpg";

        //public Func<string, string> f;
        Task dwnld;

        public bool ChkTask
        {
            get
            {
                return dwnld.IsCompleted;
            }
        }

        public event Action<string> ImageStarted;
        public event Action<string> ImageCopleted;

        public void Download(string remoteUri)
        {
            var myWebClient = new WebClient();

            if (ImageStarted != null)
                //Console.WriteLine("Качаю \"{0}\" из \"{1}\" .......\n\n", fileName, remoteUri);
                ImageStarted($"Качаю \"{fileName}\" из \"{remoteUri}\" .......\n\n");


            //myWebClient.DownloadFile(remoteUri, fileName);
            dwnld = myWebClient.DownloadFileTaskAsync(remoteUri, fileName);

            if (ImageCopleted != null)
                //Console.WriteLine("Успешно скачал \"{0}\" из \"{1}\"", fileName, remoteUri);
                ImageCopleted($"Успешно скачал \"{fileName}\" из \"{remoteUri}\"");
        }

        public void ImgStrt(string msg)
        {
            Console.WriteLine(msg);          
        }

        public void ImgCmplt(string msg)
        {
            Console.WriteLine(msg);
        }
    }

    public partial class MainWindow : Window
    {
        ImageDownloader m = new ImageDownloader();

        public MainWindow()
        {
            m.ImageStarted += FormImgStrt;
            m.ImageCopleted += FormImgCmplt;       

            InitializeComponent();
        }

        private void BtnStartDownload_Click(object sender, RoutedEventArgs e)
        {
            m.Download("https://mobimg.b-cdn.net/v3/fetch/70/708698fd251a43214d6198c0c6438156.jpeg");
           
        }

        private void BtnCurrentState_Click(object sender, RoutedEventArgs e)
        {
            if (m.ChkTask)
                this.TxtCurrentState.Text = "Task true - already download";
            else
                this.TxtCurrentState.Text = "Task false - not download";
        }

        public void FormImgStrt(string msg)
        {
            this.TxtStartDownload.Text = msg;
        }

        public void FormImgCmplt(string msg)
        {
            this.TxtCurrentState.Text = msg;
        }
    }
}
